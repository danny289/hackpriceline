require 'open-uri'
require 'net/http' 
require 'json'

class OutputController < ApplicationController
  def is_i? (str)
       !!(str =~ /\A[-+]?[0-9]+\z/)
  end

  def error
  	render "error"
  end

  def index
	@zipList=[]
	@cList=[]
	@startZip=0
	@yBegin=params[:yPos]
	
	if (is_i?(params[:homezip]))
		@startZip=params[:homezip]
	else 
		redirect_to :action => :error and return
	end

	if (is_i?(params[:zip1]))
		@zipList.push(params[:zip1])
		if (params[:c1] == "")
			@cList.push("College 1")
		else
			@cList.push(params[:c1])
		end
	else 
		if (params[:zip1]=="")
		else
		redirect_to :action => :error and return
		end
	end

	if (is_i?(params[:zip2]))
		@zipList.push(params[:zip2])
		if (params[:c2] == "")
			@cList.push("College 2")
		else
			@cList.push(params[:c2])
		end
	else 
		if (params[:zip2]=="")
		else
		redirect_to :action => :error and return
		end
	end

	if (is_i?(params[:zip3]))
		@zipList.push(params[:zip3])
		if (params[:c3] == "")
			@cList.push("College 3")
		else
			@cList.push(params[:c3])
		end
	else 
		if (params[:zip3]=="")
		else
		redirect_to :action => :error and return
		end
	end
	if (is_i?(params[:zip4]))
		@zipList.push(params[:zip4])
		if (params[:c4] == "")
			@cList.push("College 4")
		else
			@cList.push(params[:c4])
		end
	else 
		if (params[:zip4]=="")
		else
		redirect_to :action => :error and return
		end
	end

=begin
	Hit google maps api to get list of zipCodes in order (not necessarily every college zipcode)
	Call python server to get best priceline hotel and return the hotel address
=end
	url = 'https://maps.googleapis.com/maps/api/directions/json?' # trailing slash is important
	key="AIzaSyDgj_F4IN2TFhJYq79W8Z2NMj7LyzMdxbg"

	origin= @startZip
	destination= @startZip
	url2= url+ "origin="+origin+"&destination="+destination+"&waypoints=optimize:true"
	@zipList.each{ |i| url2=url2+"%7c"+i}
	url2= url2+"&key="+key

       uri = URI.parse(url2)
	   res = Net::HTTP.get_response(uri)
       response= JSON.parse(res.body)

    @duration=[]
    @address=[]

    response['routes'][0]['legs'].each{|leg| 
    	@duration.push (leg['duration']['text'])
    	@address.push(leg['end_address'])
    }
    #PRICELINE
    @hotelList=[] # in order of zipcodes list

       checkInStr = "20141001"
   		checkOutStr = "20141003"
    	numRooms = "1"
    	pageSize = "50"
    	maxPrice = params[:price]
    	sort = "1"

    urlPriceLine= "http://www.priceline.com/svcs/ac/index/hotels/"
    @zipList.each{|i| 
    	urlPriceLine2= urlPriceLine+ i
		uri = URI.parse(urlPriceLine2)
    	res = Net::HTTP.get_response(uri)
       response= JSON.parse(res.body)
        cityID= response['searchItems'][1]['cityID']

        uri2 =URI.parse("http://www.priceline.com/api/hotelretail/listing/v3/"+cityID+"/"+checkInStr+"/"+checkOutStr+"/"+numRooms+"/"+pageSize+"?"+"offset=0&"+"maxPrice="+maxPrice+"&"+"sort="+sort)
        res = Net::HTTP.get_response(uri2)
        response= JSON.parse(res.body) #all the hotels tho
        #puts response

        key, value = response['hotels'].first
		name1=value['hotelName']
		price1 =value['merchPrice']

		value=response['hotels'].values[1]
		name2 =value['hotelName']
        price2 =value['merchPrice']

        hotel1= "1. "+name1+", Price: $"+ price1.to_s
    	hotel2= "2. "+name2+", Price: $"+ price2.to_s
    	@hotelList.push(hotel1)
    	@hotelList.push(hotel2)

		@gMap="https://www.google.com/maps/embed/v1/directions?key=AIzaSyDgj_F4IN2TFhJYq79W8Z2NMj7LyzMdxbg&origin="+origin+"&destination="+destination+"&waypoints=optimize:true"
		@zipList.each{ |i| @gMap=@gMap+"%7c"+i}


    }

  	render "index"
  end

end

